import { combineReducers } from "redux"
import logged from './logged'
import {employees} from '../actions/api/employees_api'
import {clients} from '../actions/api/clients_api'
import {cars} from '../actions/api/car_api'
import {sold} from '../actions/api/sold_api'
import {providers} from '../actions/api/provider_api'
import {brands} from '../actions/api/brand_api'
import {orders} from '../actions/api/orders_api'
import {cities} from '../actions/api/city_api'

const allRedusers = combineReducers({
    logged,
    employees,
    cars,
    clients,
    sold,
    providers,
    orders,
    brands,
    cities
});

export default allRedusers