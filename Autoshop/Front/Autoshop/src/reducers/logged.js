export default function () {
    return {
        logId: localStorage.getItem('logId'),
        logShopId: localStorage.getItem('logShopId'),
        logPossitionId: localStorage.getItem('logPossitionId'),
    }
}