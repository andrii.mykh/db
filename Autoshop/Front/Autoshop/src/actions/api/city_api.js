import {Config} from "./config";

export const cities = getCity;

async function getCity() {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    return fetch(`${Config.api_ip}/Cities`, options)
        .then(response => response.json())
        .catch(console.log)
}