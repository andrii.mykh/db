import {Config} from "./config";

export const providers = getProviders;

async function getProviders() {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    return fetch(`${Config.api_ip}/ProviderInformation`, options)
        .then(response => response.json())
        .catch(console.log)
}

