import {Config} from "./config";

export const orders = getOrders;

async function getOrders() {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    return fetch(`${Config.api_ip}/OrdersInformation`, options)
        .then(response => response.json())
        .catch(console.log)
}