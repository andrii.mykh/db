import {Config} from "./config";

export const employees = getEmployees;

async function getEmployees() {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    return fetch(`${Config.api_ip}/Employees`, options)
        .then(response => response.json())
        .catch(console.log)
}

