import {Config} from "./config";

export const brands = getBrand;

async function getBrand() {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    return fetch(`${Config.api_ip}/Brands`, options)
        .then(response => response.json())
        .catch(console.log)
}