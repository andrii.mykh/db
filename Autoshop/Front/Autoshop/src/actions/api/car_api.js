import {Config} from "./config";

export const cars = getCars;

async function getCars() {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    return fetch(`${Config.api_ip}/CarInformation`, options)
        .then(response => response.json())
        .catch(console.log)
}

