import {Config} from "./config";

export const clients = getClients;

async function getClients() {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    return fetch(`${Config.api_ip}/Clients`, options)
        .then(response => response.json())
        .catch(console.log)
}

