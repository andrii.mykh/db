import {Config} from "./config";

export const sold = getSold;

async function getSold() {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    return fetch(`${Config.api_ip}/SellInformation`, options)
        .then(response => response.json())
        .catch(console.log)
}