import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Config} from "../actions/api/config";
import Select from 'react-select';

class AddProvider extends Component {

    constructor(props) {
        super(props)
    
        this.addProvider = this.addProvider.bind(this)

        this.state = {
            brands:[],
            cities:[],

            person: '',
            brand: null,
            city: null,
            address: '',
            email: '',
            telephone: ''
        }
    }
    

    async componentDidMount(){
        if(!this.props.log.logId){
            this.props.history.push('/login');
        }

        let resultB = await this.props.brands
        this.setState({brands: resultB.map(e => ({value: e.idBrand, label: e.brand1}))})
        console.log(this.state.brands)

        let resultC = await this.props.cities
        this.setState({cities : resultC.map(e => ({value: e.idCity, label: e.city1}))})
        console.log(this.state.cities)
    }

    componentWillUnmount()
    {
        window.location.reload()
    }

    handleChangeB = brand => {
        this.setState({ brand });
        console.log(`Brand selected:`, brand);
      };

    handleChangeC = city => {
      this.setState({ city });
      console.log(`City selected:`, city);
    };

    addProvider(){
        if(this.state.person&&this.state.brand&&this.state.city&&this.state.address&&this.state.email&&this.state.telephone)
        {if(window.confirm('Do You check that in this city provides this brand and has this address?')){
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(this.state.person+'~'+this.state.brand.value+'~'+this.state.city.value+'~'+
                                this.state.address+'~'+this.state.email+'~'+this.state.telephone)
        };
        fetch(`${Config.api_ip}/Providers`, options)
        .catch(console.log)
        alert(`${this.state.person} added.`)
        window.location.reload();}}
        else{alert('Fill new provider data')}
    }

    render() {
        const { brand } = this.state;
        const { city } = this.state;
        return (
            <div className="AddProvider">
                <div class="col-sm-12">

                <input
                    type="text"
                    name="person"
                    placeholder="Person`s name"
                    value={this.state.person}
                    onChange={e => this.setState({person: e.target.value})}
                    required
                    class="form-control mb-2 mr-sm-2 col-sm-8"
                />
                <div class="col-sm-8 mb-2" >
                    <Select placeholder='Brand' value={brand} onChange={this.handleChangeB} options={this.state.brands}/>
                    <div class="mb-2 mr-sm-2"/>
                    <Select placeholder='City' value={city} onChange={this.handleChangeC} options={this.state.cities}/>
                </div>
                <input
                    type="text"
                    name="address"
                    placeholder="Address"
                    value={this.state.address}
                    onChange={e => this.setState({address: e.target.value})}
                    required
                    class="form-control mb-2 mr-sm-2 col-sm-8"
                />
                <input
                    type="email"
                    name="email"
                    placeholder="E-mail"
                    value={this.state.email}
                    onChange={e => this.setState({email: e.target.value})}
                    required
                    class="form-control mb-2 mr-sm-2 col-sm-8"
                />

                <input
                    type="text" pattern=" [0-9]*"
                    name="phone"
                    placeholder="Phone"
                    value={this.state.telephone}
                    onChange={e => this.setState({telephone: e.target.value})}
                    required
                    class="form-control mb-2 mr-sm-2 col-sm-8"
                />
                </div>
                <button type="submit" onClick={this.addProvider} onSubmit={e=>e.preventDefault()} class="btn btn-primary mb-2 col-sm-1">Add</button>

            </div>
        )
    }
}

function mapStateToProps(state)
{
    return{
        log: state.logged,
        brands: state.brands,
        cities: state.cities
    }
}

export default connect(mapStateToProps)(AddProvider)