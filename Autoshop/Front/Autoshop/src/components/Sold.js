import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table-6';

class Sold extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            sold: [],
        }
    }
    

    async componentDidMount(){
        if(!this.props.log.logId){
            this.props.history.push('/login');
        }

        let resultOr = await this.props.sold

        let r0 = resultOr.map(e => {if(e.shop==this.props.log.logShopId) return e;})
        this.setState({sold: r0.filter(function (el) {return el != null;})})
        console.log(this.state.sold)
    }

    componentWillUnmount()
    {
        window.location.reload()
    }

    render() {

        const columns =[
            {
                Header: "Sold cars",
                columns: [
                    {  
                       Header: 'Employee',  
                       accessor: 'employeeInfo'  
                       },{  
                        Header: 'Client',  
                        accessor: 'client'  
                        },{  
                            Header: 'Car',  
                            accessor: 'car'  
                            },{  
                                Header: 'Date',  
                                accessor: 'date'  
                                },{  
                                    Header: 'Total',  
                                    accessor: 'total'  
                                    }
                ]
            }
        ];

        return (
            <div class="col-sm-12">
                <ReactTable
                data={this.state.sold}
                columns={columns}  
                defaultPageSize = {10}  
                pageSizeOptions = {[10,20,30]} 
                filterable
                filtered={this.state.filtered}
                onFilteredChange={(filtered, column, value) => {
                    this.onFilteredChangeCustom(value, column.id || column.accessor);
                }}
                defaultFilterMethod={(filter, row, column) => {
                    const id = filter.pivotId || filter.id;
                    if (typeof filter.value === "object") {
                    return row[id] !== undefined
                        ? filter.value.indexOf(row[id]) > -1
                        : true;
                    } else {
                    return row[id] !== undefined
                        ? String(row[id]).indexOf(filter.value) > -1
                        : true;
                    }
                }}
                />
                <br/>
            </div>
        )
    }
}

function mapStateToProps(state)
{
    return{
        log: state.logged,
        sold: state.sold
    }
}

export default connect(mapStateToProps)(Sold)