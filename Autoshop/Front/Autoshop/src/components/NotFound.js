import React, { Component } from 'react';

export class NotFound extends Component {
    static displayName = NotFound.name;

    componentWillUnmount()
    {
        window.location.reload()
    }

    render() {
        return (
            <div>
                <h1>Page not found</h1>
            </div>
        );
    }
}