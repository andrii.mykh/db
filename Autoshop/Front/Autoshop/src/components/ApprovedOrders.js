import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table-6';

class ApprovedOrders extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            taken_orders: [],
            visibleT: false,

            not_taken: [],
            visibleN: false
        }
    }
    

    async componentDidMount(){
        if(!this.props.log.logId){
            this.props.history.push('/login');
        }

        let resultOr = await this.props.orders

        let r0 = resultOr.map(e => {if(e.shop==this.props.log.logShopId && e.ifAdded === 0) return e;})
        this.setState({not_taken: r0.filter(function (el) {return el != null;})})
        console.log(this.state.not_taken)

        let r1 = resultOr.map(e => {if(e.shop==this.props.log.logShopId && e.ifAdded === 1) return e;})
        this.setState({taken_orders: r1.filter(function (el) {return el != null;})})
        console.log(this.state.taken_orders)
    }

    componentWillUnmount()
    {
        window.location.reload()
    }

    render() {

        const columns =[
            {
                Header: "Orders",
                columns: [
                    {  
                       Header: 'Employee',  
                       accessor: 'employeeInfo'  
                       },{  
                        Header: 'Provider',  
                        accessor: 'provider'  
                        },{  
                            Header: 'Car',  
                            accessor: 'car'  
                            },{  
                            Header: 'Amount',  
                            accessor: 'amount'  
                            },{  
                                Header: 'Price',  
                                accessor: 'price'  
                                },{  
                                    Header: 'Date',  
                                    accessor: 'date'  
                                    },{  
                                        Header: 'Approved',  
                                        accessor: 'addDate'  
                                        }
                ]
            }
        ];

        return (
            <div class="col-sm-12">
                <button type="submit" onClick={() => {var value = this.state.visibleT; this.setState({visibleT: !value})}}
                onSubmit={e=>e.preventDefault()} class="btn btn-primary mr-sm-5 mb-sm-5" style={{height: 50}}>Taken orders</button>
                <br/>
                {this.state.visibleT ? 
                <ReactTable
                data={this.state.taken_orders}
                columns={columns}  
                defaultPageSize = {10}  
                pageSizeOptions = {[10,20,30]} 
                filterable
                filtered={this.state.filtered}
                onFilteredChange={(filtered, column, value) => {
                    this.onFilteredChangeCustom(value, column.id || column.accessor);
                }}
                defaultFilterMethod={(filter, row, column) => {
                    const id = filter.pivotId || filter.id;
                    if (typeof filter.value === "object") {
                    return row[id] !== undefined
                        ? filter.value.indexOf(row[id]) > -1
                        : true;
                    } else {
                    return row[id] !== undefined
                        ? String(row[id]).indexOf(filter.value) > -1
                        : true;
                    }
                }}
                />
                : null}
                <br/>
                <button type="submit" onClick={() => {var value = this.state.visibleN; this.setState({visibleN: !value})}}
                onSubmit={e=>e.preventDefault()} class="btn btn-primary mr-sm-5 mb-sm-5" style={{height: 50}}>Not taken</button>
                {this.state.visibleN ? 
                <ReactTable
                data={this.state.not_taken}
                columns={columns}  
                defaultPageSize = {10}  
                pageSizeOptions = {[10,20,30]} 
                filterable
                filtered={this.state.filtered}
                onFilteredChange={(filtered, column, value) => {
                    this.onFilteredChangeCustom(value, column.id || column.accessor);
                }}
                defaultFilterMethod={(filter, row, column) => {
                    const id = filter.pivotId || filter.id;
                    if (typeof filter.value === "object") {
                    return row[id] !== undefined
                        ? filter.value.indexOf(row[id]) > -1
                        : true;
                    } else {
                    return row[id] !== undefined
                        ? String(row[id]).indexOf(filter.value) > -1
                        : true;
                    }
                }}
                />
                : null}
                <br/>
            </div>
        )
    }
}

function mapStateToProps(state)
{
    return{
        log: state.logged,
        orders: state.orders
    }
}

export default connect(mapStateToProps)(ApprovedOrders)