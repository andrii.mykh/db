import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Config} from "../actions/api/config";
import ReactTable from 'react-table-6';
import "react-table-6/react-table.css";

class ApproveOrder extends Component {

    constructor(props) {
        super(props)

        this.toggleAdd = this.toggleAdd.bind(this);
        this.toggleNot = this.toggleNot.bind(this);
        this.state = {
             orders: []
        }
    }
    

    async componentDidMount(){
        if(!this.props.log.logId){
            this.props.history.push('/login');
        }
        let resultOr = await this.props.orders
        let r = resultOr.map(e => {if(e.shop==this.props.log.logShopId && e.employee==this.props.log.logId && e.ifAdded === null) return e;})
        this.setState({orders: r.filter(function (el) {return el != null;})})
        console.log(this.state.orders)
    }

    componentWillUnmount()
    {
        window.location.reload()
    }

    onFilteredChangeCustom = (value, accessor) => {
        let filtered = this.state.filtered;
        let insertNewFilter = 1;
    
        if (filtered.length) {
          filtered.forEach((filter, i) => {
            if (filter["id"] === accessor) {
              if (value === "" || !value.length) filtered.splice(i, 1);
              else filter["value"] = value;
    
              insertNewFilter = 0;
            }
          });
        }
    
        if (insertNewFilter) {
          filtered.push({ id: accessor, value: value });
        }
    
        this.setState({ filtered: filtered });
      };

    toggleAdd(id){
        
        if(window.confirm('You want take this order, don`t you?')){
            const options = {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify(1)
            };
            console.log(options, id)
            fetch(`${Config.api_ip}/Orders/${id}`, options)
            .catch(console.log)
            window.location.reload();
            }
    }

    toggleNot(id){
        
        if(window.confirm('You don`t want take this order, do you?')){
        const options = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(0)
        };
        console.log(options, id)
        fetch(`${Config.api_ip}/Orders/${id}`, options)
        .catch(console.log)
        window.location.reload();
        }
    }

    render() {

        const columns =[
            {
                Header: "Orders",
                columns: [
                    {
                        id: "buttonAdd",
                        accessor: "",
                        Cell: ({ original }) => {
                            return (
                                <button
                                style={{width: 60, minBlockSize:20}}
                                value = "Add"
                                type="submit"
                                onClick={() => this.toggleAdd(original.id)}
                                >Add</button>
                            );
                        },
                        Header: 'Add',
                        filterable: false,
                        width: 75
                    },
                    {
                        id: "buttonNot",
                        accessor: "",
                        Cell: ({ original }) => {
                            return (
                                <button
                                style={{width: 60, minBlockSize:20}}
                                value = "Delete"
                                type="submit"
                                onClick={() => this.toggleNot(original.id)}
                                >Delete</button>
                            );
                        },
                        Header: 'Delete',
                        filterable: false,
                        width: 75
                    },
                    {  
                       Header: 'Employee',  
                       accessor: 'employeeInfo'  
                       },{  
                        Header: 'Provider',  
                        accessor: 'provider'  
                        },{  
                            Header: 'Car',  
                            accessor: 'car'  
                            },{  
                            Header: 'Amount',  
                            accessor: 'amount'  
                            },{  
                                Header: 'Price',  
                                accessor: 'price'  
                                },{  
                                    Header: 'Date',  
                                    accessor: 'date'  
                                    },{  
                                        Header: 'Description',  
                                        accessor: 'description'  
                                        }
                ]
            }
        ];

        return (
            <div>
                <ReactTable
                    data={this.state.orders}
                    columns={columns}  
                    defaultPageSize = {10}  
                    pageSizeOptions = {[10,20,30]} 
                    filterable
                    filtered={this.state.filtered}
                    onFilteredChange={(filtered, column, value) => {
                        this.onFilteredChangeCustom(value, column.id || column.accessor);
                    }}
                    defaultFilterMethod={(filter, row, column) => {
                        const id = filter.pivotId || filter.id;
                        if (typeof filter.value === "object") {
                        return row[id] !== undefined
                            ? filter.value.indexOf(row[id]) > -1
                            : true;
                        } else {
                        return row[id] !== undefined
                            ? String(row[id]).indexOf(filter.value) > -1
                            : true;
                        }
                    }}
                />
            </div>
        )
    }
}

function mapStateToProps(state)
{
    return{
        log: state.logged,
        orders: state.orders
    }
}

export default connect(mapStateToProps)(ApproveOrder)