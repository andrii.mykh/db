import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table-6';
import "react-table-6/react-table.css";
import Receipt from '../containers/Receipt'

class SellCar extends Component {

    constructor(props) {
        super(props)

        this.toggleRowCar = this.toggleRowCar.bind(this);
        this.toggleRowClient = this.toggleRowClient.bind(this);
        this.createReceipt = this.createReceipt.bind(this);

        this.state = {
            clients: [],
            cars: [],

            filtered: [],

            selectedCar: {}, 

            selectedClient: {},

            receipt: null
        }

    }
    

    async componentDidMount(){
        if(!this.props.log.logId){
            this.props.history.push('/login');
        }
        const resultCl = await this.props.clients
        this.setState({clients: resultCl.map(e => e)})
        console.log(this.state.clients)
        const resultCr = await this.props.cars
        let r = resultCr.map(e => {if(e.shop==this.props.log.logShopId&&e.amount>0) return e;})
        this.setState({cars: r.filter(function (el) {return el != null;})})
        console.log(this.state.cars)
    }

    componentWillUnmount()
    {
        window.location.reload()
    }

    toggleRowCar(firstName) {
		const newSelected = Object.assign({}, this.state.selectedCar);
        newSelected[firstName] = !this.state.selectedCar[firstName];
        console.log(newSelected)
		this.setState({
			selectedCar: newSelected,
		});
    }
    
    toggleRowClient(firstName) {
        const newSelected = Object.assign({}, this.state.selectedClient);
        this.state.clients.forEach(x => {
            newSelected[x.idClient] = false;
        });
        newSelected[firstName] = !this.state.selectedClient[firstName];
        console.log(newSelected)
		this.setState({
			selectedClient: newSelected,
		});
	}

    onFilteredChangeCustom = (value, accessor) => {
        let filtered = this.state.filtered;
        let insertNewFilter = 1;
    
        if (filtered.length) {
          filtered.forEach((filter, i) => {
            if (filter["id"] === accessor) {
              if (value === "" || !value.length) filtered.splice(i, 1);
              else filter["value"] = value;
    
              insertNewFilter = 0;
            }
          });
        }
    
        if (insertNewFilter) {
          filtered.push({ id: accessor, value: value });
        }
    
        this.setState({ filtered: filtered });
      };

    createReceipt()
    {
        var car = false
        var client = null
        var car_storages = []

        for (let key in this.state.selectedCar) {if(this.state.selectedCar[key]) {car = true; car_storages.push(key);}}
        for (let key in this.state.selectedClient) {if(this.state.selectedClient[key]) {client = key; break;}}
        if(car && client)
        {
            var choosed_cars = []          
            for(let storage in car_storages)
            {
                for (let car in this.state.cars)
                {
                    if(this.state.cars[car].storageId == car_storages[storage]) 
                    {choosed_cars.push(this.state.cars[car]); break;}
                }
            }
            var choosed_client = this.state.clients.map(c => {if (c.idClient==client){return c;}}).filter(function (el) {
                return el != null;
              })[0]
            this.setState({receipt: <Receipt id={this.props.log.logId} cars={choosed_cars} client={choosed_client} shop={this.props.log.logShopId}/>})
        }
        else {
            alert('Some mistake. Check choosed cars and client')
        }
    }

    render() {

        const car_columns =[
            {
                Header: "Cars",
                columns: [
                    {
                        id: "checkbox",
                        accessor: "",
                        Cell: ({ original }) => {
                            return (
                                <input
                                    style={{width: 40, minBlockSize:20}}
                                    type="checkbox"
                                    className="checkbox"
                                    checked={this.state.selectedCar[original.storageId] === true}
                                    onChange={() => this.toggleRowCar(original.storageId)}
                                />
                            );
                        },
                        Header: x => {
                            return (
                                <input
                                    style={{width: 40, minBlockSize:20}}
                                    type="checkbox"
                                    className="checkbox"
                                    checked="true"
                                    disabled
                                />
                            );
                        },
                        filterable: false,
                        width: 45
                    },
                    {  
                       Header: 'Brand',  
                       accessor: 'brand'  
                       },{  
                        Header: 'Model',  
                        accessor: 'model'  
                        },{  
                            Header: 'Price',  
                            accessor: 'price'  
                            },{  
                            Header: 'Year',  
                            accessor: 'year'  
                            },{  
                                Header: 'Color',  
                                accessor: 'color'  
                                },{  
                                    Header: 'Types',  
                                    accessor: 'types'  
                                    },
                                    {  
                                        Header: 'Amount',  
                                        accessor: 'amount',
                                        filterable: false,  
                                        }
                ]
            }
        ];

        const client_columns = [{
            Header: "Clients",
            columns: [
                {
                    id: "checkbox",
                    accessor: "",
                    Cell: ({ original }) => {
                        return (
                            <input
                                style={{width: 40, minBlockSize:20}}
                                type="checkbox"
                                className="checkbox"
                                checked={this.state.selectedClient[original.idClient] === true}
                                onChange={() => this.toggleRowClient(original.idClient)}
                            />
                        );
                    },
                    Header: x => {
                        return (
                            <input
                                style={{width: 40, minBlockSize:20}}
                                type="checkbox"
                                className="checkbox"
                                checked="true"
                                disabled
                            />
                        );
                    },
                    filterable: false,
                    width: 45
                },
                {  
                   Header: 'Name',  
                   accessor: 'fullname'  
                   },{  
                    Header: 'Birth',  
                    accessor: 'birth',
                    filterable: false,  
                    },{  
                        Header: 'Discount',  
                        accessor: 'discount',
                        filterable: false,  
                        },{  
                        Header: 'Passport',  
                        accessor: 'passport',
                        filterable: false,  
                        }
            ]
        }];

        return (
            <div>
                <ReactTable
                    data={this.state.cars}
                    columns={car_columns}  
                    defaultPageSize = {10}  
                    pageSizeOptions = {[10,20,30]} 
                    filterable
                    filtered={this.state.filtered}
                    onFilteredChange={(filtered, column, value) => {
                        this.onFilteredChangeCustom(value, column.id || column.accessor);
                    }}
                    defaultFilterMethod={(filter, row, column) => {
                        const id = filter.pivotId || filter.id;
                        if (typeof filter.value === "object") {
                        return row[id] !== undefined
                            ? filter.value.indexOf(row[id]) > -1
                            : true;
                        } else {
                        return row[id] !== undefined
                            ? String(row[id]).indexOf(filter.value) > -1
                            : true;
                        }
                    }}
                />  
                <br/>
                <div class="row mb-sm-5">
                <ReactTable class="col-sm-6"
                    data={this.state.clients}
                    columns={client_columns}  
                    defaultPageSize = {10}  
                    pageSizeOptions = {[10,20,30]} 
                    filterable
                    filtered={this.state.filtered}
                    onFilteredChange={(filtered, column, value) => {
                        this.onFilteredChangeCustom(value, column.id || column.accessor);
                    }}
                    defaultFilterMethod={(filter, row, column) => {
                        const id = filter.pivotId || filter.id;
                        if (typeof filter.value === "object") {
                        return row[id] !== undefined
                            ? filter.value.indexOf(row[id]) > -1
                            : true;
                        } else {
                        return row[id] !== undefined
                            ? String(row[id]).indexOf(filter.value) > -1
                            : true;
                        }
                    }}
                />
                <div class="row ml-sm-5 mr-sm-5 col-sm-6">
                    <button type="submit" onClick={this.createReceipt} onSubmit={e=>e.preventDefault()} class="btn btn-primary mr-sm-5 col-sm-2" style={{height: 50}}>Receipt</button>
                    <div class="col-sm-9">{this.state.receipt}</div>
                </div>
                </div>          
            </div>
        );
    }
}

function mapStateToProps(state)
{
    return{
        log: state.logged,
        cars: state.cars,
        clients: state.clients,
    }
}

export default connect(mapStateToProps)(SellCar)