import React, { Component } from 'react'
import { connect } from 'react-redux';

class Login extends Component {

    constructor(props) {
        super(props);
        this.submitUser = this.submitUser.bind(this);
        this.logout = this.logout.bind(this);
        this.state={
            name:'',
            passport:'',
            employees: null
        }
    }
  
    logout()
    {
        localStorage.removeItem('logId')
        this.props.log.logId = localStorage.getItem('logId')
        localStorage.removeItem('logShopId')
        this.props.log.logShopId = localStorage.getItem('logShopId')
        localStorage.removeItem('logPossitionId')
        this.props.log.logPossitionId = localStorage.getItem('logPossitionId')
        localStorage.removeItem('EmployeeInfo')
    }

    componentWillMount()
    {
        this.logout();
    }

    async componentDidMount() {
        const result = await this.props.employees
        this.setState({employees: result})

    }

    componentWillUnmount()
    {
        window.location.reload()
    }
    
    handleSubmit(event) {
        event.preventDefault();
      }
      
    submitUser()
    {
        let data = this.state.employees.map(e =>{if(e.fullname==this.state.name && e.passport==this.state.passport) return e;})
        
        let employee = data.filter(function (el) {
            return el != null;
          })[0];
        
        if(!employee) {
            alert('Uncorrect login. Check name and passport!')
        }
        else {
        
        localStorage.setItem('logId', employee.idEmployee)
        this.props.log.logId = localStorage.getItem('logId')
        localStorage.setItem('logShopId', employee.idShop)
        this.props.log.logShopId = localStorage.getItem('logShopId')
        localStorage.setItem('logPossitionId', employee.idPosition)
        this.props.log.logPossitionId = localStorage.getItem('logPossitionId')

        this.props.history.push('/');}
    }

    render() {

        return (
            <div className="Login">
            {console.log(this.props.employees)}
            <form onSubmit={this.handleSubmit} class="form-inline row">
                <input
                    type="text" 
                    name="name"
                    placeholder="Fullmame"
                    value={this.state.name}
                    onChange={e => this.setState({name: e.target.value})}
                    required
                    class="form-control mb-2 mr-sm-2 col-sm-5"
                />

                <input
                    type="password" pattern="[0-9]*"
                    name="passport"
                    placeholder="Passport"
                    value={this.state.passport}
                    onChange={e => this.setState({passport: e.target.value})}
                    required
                    class="form-control mb-2 mr-sm-2 col-sm-5"
                />

                <button type="submit" onClick={this.submitUser} onSubmit={e=>e.preventDefault()} class="btn btn-primary mb-2 col-sm-1">Login</button>
            </form>
            </div>
        )
    }
}

function mapStateToProps(state/*, props*/)
{
    return{
        employees: state.employees,
        log: state.logged
    }
}

export default connect(mapStateToProps)(Login)