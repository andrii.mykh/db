import React, { Component } from 'react';
import { connect } from 'react-redux';
import EmployeeInfo from '../containers/EmployeeInfo';
import SelectContainer from '../containers/SelectContainer';

class Home extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            sell_options: [
                { value: 'sell_car', label: 'Sell car' },
                { value: 'sold', label: 'Sold cars' },
                { value: 'add_client', label: 'Add client' },
            ],
            order_options: [
                { value: 'make_order', label: 'Make order' },
                { value: 'approve_order', label: 'Approve order' },
                { value: 'approved_orders', label: 'Approved orders' },
            ],
            provider_options: [
                { value: 'add_provider', label: 'Add provider' },
            ]
        }
    }
   
    componentDidMount(){
        //console.log('h m')  
        if(!this.props.log.logId){
            this.props.history.push('/login');
        }
    }

    componentWillUnmount()
    {
            window.location.reload()
    }

    render() {
        
        return (
            <div>
                <EmployeeInfo/>
                <br/>
                <SelectContainer label = "Selling" options = {this.state.sell_options}/>
                <br/>
                {(() => {
                    var value = []
                    if(this.props.log.logPossitionId >= 2)
                        value.push(
                            <SelectContainer label = "Ordres" options = {this.state.order_options}/>)
                    value.push(<br/>)
                    if(this.props.log.logPossitionId == 3)
                        value.push(
                            <SelectContainer label = "Providers" options = {this.state.provider_options}/>)  
                    return value  
                })()}
            </div>
        )
    }
}

function mapStateToProps(state)
{
    return{
        log: state.logged
    }
}

export default connect(mapStateToProps)(Home)