import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table-6';
import "react-table-6/react-table.css";
import ProviderTable from '../containers/ProviderTable';

class MakeOrder extends Component {

    constructor(props) {
        super(props)
    
        this.toggleRowCar = this.toggleRowCar.bind(this);
        this.chooseProvider = this.chooseProvider.bind(this);

        this.state = {
            cars: [],
            cars_in_shop: [],
            providers: [],

            filtered: [],

            selectedCar: {}, 

            providersTable: null
        }
    }
    

    async componentDidMount(){
        if(!this.props.log.logId){
            this.props.history.push('/login');
        } 
        let resultCr = await this.props.cars
        let r = resultCr.map(e => {if(e.shop==this.props.log.logShopId) return e;})
        this.setState({cars_in_shop: r.filter(function (el) {return el != null;})})
        console.log(this.state.cars_in_shop)

        let newArray = []; 
        // Declare an empty object 
        let uniqueObject = {};              
        // Loop for the array elements 
        for (let i in resultCr) { 
            // Extract the title 
            let objTitle = resultCr[i]['car']; 
            // Use the title as the index 
            uniqueObject[objTitle] = resultCr[i]; 
        } 
        // Loop to push unique object into array 
        for (let i in uniqueObject) { 
            newArray.push(uniqueObject[i]); 
        } 
        this.setState({cars: newArray.map(e => e)})
        console.log(this.state.cars)

        let resultP = await this.props.providers
        this.setState({providers: resultP.map(e => e)})
        console.log(this.state.providers)
    }

    componentWillUnmount()
    {
        window.location.reload()
    }

    toggleRowCar(firstName) { 
        this.setState({providersTable: null})      
        const newSelected = Object.assign({}, this.state.selectedCar);
        this.state.cars.forEach(x => {
            newSelected[x.car] = false;
        });
        newSelected[firstName] = !this.state.selectedCar[firstName];
        console.log(newSelected)
		this.setState({
			selectedCar: newSelected,
		});
    }

    onFilteredChangeCustom = (value, accessor) => {
        let filtered = this.state.filtered;
        let insertNewFilter = 1;
    
        if (filtered.length) {
          filtered.forEach((filter, i) => {
            if (filter["id"] === accessor) {
              if (value === "" || !value.length) filtered.splice(i, 1);
              else filter["value"] = value;
    
              insertNewFilter = 0;
            }
          });
        }
    
        if (insertNewFilter) {
          filtered.push({ id: accessor, value: value });
        }
    
        this.setState({ filtered: filtered });
      };

    chooseProvider()
    {
        var car = null
        for (let key in this.state.selectedCar) {if(this.state.selectedCar[key]) {car = key; break;}}
        console.log(car)
        var choosed_car = this.state.cars.map(c => {if (c.car==car){return c;}}).filter(function (el) {
            return el != null;
          })[0]
        if(car)
        {
            var providers = this.state.providers.filter(e => {return e['brand']==choosed_car['brand']})
            console.log(providers)
            this.setState({providersTable: <ProviderTable providers = {providers} car = {choosed_car} 
                storages = {this.state.cars_in_shop} shop = {this.props.log.logShopId}/>})
        }
        else {alert('Choose car!')}
    }

    render() {

        const car_columns =[
            {
                Header: "Cars",
                columns: [
                    {
                        id: "checkbox",
                        accessor: "",
                        Cell: ({ original }) => {
                            return (
                                <input
                                    style={{width: 40, minBlockSize:20}}
                                    type="checkbox"
                                    className="checkbox"
                                    checked={this.state.selectedCar[original.car] === true}
                                    onChange={() => this.toggleRowCar(original.car)}
                                />
                            );
                        },
                        Header: x => {
                            return (
                                <input
                                    style={{width: 40, minBlockSize:20}}
                                    type="checkbox"
                                    className="checkbox"
                                    checked="true"
                                    disabled
                                />
                            );
                        },
                        filterable: false,
                        width: 45
                    },
                    {  
                       Header: 'Brand',  
                       accessor: 'brand'  
                       },{  
                        Header: 'Model',  
                        accessor: 'model'  
                        },{  
                            Header: 'Price',  
                            accessor: 'price'  
                            },{  
                            Header: 'Year',  
                            accessor: 'year'  
                            },{  
                                Header: 'Color',  
                                accessor: 'color'  
                                },{  
                                    Header: 'Types',  
                                    accessor: 'types'  
                                    }
                ]
            }
        ];

        return (
            <div class="col-sm-12">
                <ReactTable
                    data={this.state.cars}
                    columns={car_columns}  
                    defaultPageSize = {10}  
                    pageSizeOptions = {[10,20,30]} 
                    filterable
                    filtered={this.state.filtered}
                    onFilteredChange={(filtered, column, value) => {
                        this.onFilteredChangeCustom(value, column.id || column.accessor);
                    }}
                    defaultFilterMethod={(filter, row, column) => {
                        const id = filter.pivotId || filter.id;
                        if (typeof filter.value === "object") {
                        return row[id] !== undefined
                            ? filter.value.indexOf(row[id]) > -1
                            : true;
                        } else {
                        return row[id] !== undefined
                            ? String(row[id]).indexOf(filter.value) > -1
                            : true;
                        }
                    }}
                />

                <br/>
                <button type="submit" onClick={this.chooseProvider} onSubmit={e=>e.preventDefault()} class="btn btn-primary mr-sm-5 mb-sm-5" style={{height: 50}}>Choose provider</button>
                {this.state.providersTable}
            </div>
        )
    }
}

function mapStateToProps(state)
{
    return{
        log: state.logged,
        cars: state.cars,
        providers: state.providers,
    }
}

export default connect(mapStateToProps)(MakeOrder)