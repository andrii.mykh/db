import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Config} from "../actions/api/config";

class AddClient extends Component {

    constructor(props) {
        super(props);
        this.addClient = this.addClient.bind(this)
        this.state={
            clients: [],
            name:'',
            birth: '',
            passport:'',
        }
    }

    componentDidMount(){
        if(!this.props.log.logId){
            this.props.history.push('/login');
        }
    }

    componentWillUnmount()
    {
        window.location.reload()
    }

    handleSubmit(event) {
        event.preventDefault();
      }

    addClient(){
        //we don`t check if passport added because one number can be in passport of different countries 
        //so in the future will be added this column
        if(this.state.name&&this.state.birth&&this.state.passport){
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(this.state.name+'~'+this.state.birth+'~'+this.state.passport)
        };
        fetch(`${Config.api_ip}/Clients`, options)
        .catch(console.log)
        if(this.state.name&&this.state.birth&&this.state.passport)
            alert(`${this.state.name} added.`)
        window.location.reload();} else {alert('Fill new client data')}
    }

    render() {
        return (
            <div className="AddClient">
                <div class="row">
            <form onSubmit={this.handleSubmit} class="form-inline col-sm-12">
                <input
                    type="text"
                    name="name"
                    placeholder="Fullmame"
                    value={this.state.name}
                    onChange={e => this.setState({name: e.target.value})}
                    required
                    class="form-control mb-2 mr-sm-2 col-sm-4"
                />
                {//mr-sm-1
    }
                <input
                    type="date" 
                    name="birth"
                    placeholder="Birth"
                    value={this.state.birth}
                    onChange={e => this.setState({birth: e.target.value})}
                    required
                    class="form-control mb-2 mr-sm-2 col-sm-3"
                />

                <input
                    type="text" pattern="[0-9]*"
                    name="passport"
                    placeholder="Passport"
                    value={this.state.passport}
                    onChange={e => this.setState({passport: e.target.value})}
                    required
                    class="form-control mb-2 mr-sm-2 col-sm-4"
                />
                </form>
                </div>
                <button type="submit" onClick={this.addClient} onSubmit={e=>e.preventDefault()} class="btn btn-primary mb-2 col-sm-1">Add</button>
            </div>
        )
    }
}

function mapStateToProps(state)
{
    return{
        log: state.logged
    }
}

export default connect(mapStateToProps)(AddClient)