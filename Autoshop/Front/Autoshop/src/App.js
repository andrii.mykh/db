import React from 'react';
import ReactDom from 'react-dom';
import logo from './logo.svg';
import { Container } from 'reactstrap';

import {createStore} from 'redux'
import {Provider} from 'react-redux'
import allRedusers from './reducers/index';

import {BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import Home from './components/Home'
import Login from './components/Login'
import LoginNavbar from './containers/LoginNavbar';
import AddClient from './components/AddClient';
import SellCar from './components/SellCar';
import Sold from './components/Sold';
import MakeOrder from './components/MakeOrder';
import ApproveOrder from './components/ApproveOrder';
import ApprovedOrders from './components/ApprovedOrders';
import AddProvider from './components/AddProvider';
import { NotFound } from './components/NotFound';

function App() {

  const store = createStore(allRedusers) 

  return (
    <Provider store={store}>    
      <Router>
        <div className="app">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <Link to="/" style={{ marginRight: 10 }}><h3>Home</h3></Link>
            <Link to="/login"><LoginNavbar /></Link>
          </nav>
          <div class="ml-5 mr-5 mt-5">
            <Switch>
              <Route exact path='/' component={Home}/>
              <Route exact path='/login' component={Login}/>
              <Route path='/add_client' component={AddClient}/>
              <Route path='/sell_car' component={SellCar}/>
              <Route path='/sold' component={Sold}/>  
              <Route path='/make_order' component={MakeOrder}/>
              <Route path='/approve_order' component={ApproveOrder}/>
              <Route path='/approved_orders' component={ApprovedOrders}/>
              <Route path='/add_provider' component={AddProvider}/>
              <Route path="*" component={NotFound}/>
            </Switch>
          </div>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
