import React, { Component } from 'react'
import {Config} from "../actions/api/config";

export default class Receipt extends Component {

    constructor(props) {
        super(props)
    
        this.total_price = this.total_price.bind(this)
        this.sellCars = this.sellCars.bind(this)

        this.state = {
             shop: [String]
        }
    }
    

    async componentDidMount() {
        //console.log('data')
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const result = await fetch(`${Config.api_ip}/Shops/info_${this.props.shop}`, options)
        .then(response => response.json())
        .catch(console.log)
        this.setState({shop: result})
    }

    total_price()
    {
        var price = 0
        this.props.cars.map(car => {price = price + car.price - car.price*this.props.client.discount/1000})
        return price
    }
    sellCars(){
        this.props.cars.map(car => {

            var data = new Date().toLocaleString()
            var total = car.price - car.price*this.props.client.discount/1000
            const options = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify(this.props.id+'~'+this.props.client.idClient+'~'+car.storageId
                                    +'~'+data+'~'+total)
            };
            console.log(options)
            fetch(`${Config.api_ip}/CarSales`, options)
            .catch(console.log)
        })
        window.location.reload();
    }

    render() {
        const text = JSON.parse(localStorage.getItem('EmployeeInfo'));
        return (
            <div class="col-sm-12">
            <table class="table">
                <tr>
                    <td>Brand </td>
                    <td>Model </td>
                    <td>Price </td>
                    <td>Year </td>
                    <td>Color </td>
                    <td>Types </td>
                </tr>
                {this.props.cars.map(car => {return <tr>
                    <td>{car.brand}</td>
                    <td>{car.model}</td>
                    <td>{car.price}</td>
                    <td>{car.year}</td>
                    <td>{car.color}</td>
                    <td>{car.types}</td>
                </tr>;})}

                <tr>
                    <td>Person </td>
                    <td>Birth </td>
                    <td>Discount </td>
                    <td>Passport </td>
                </tr>
                <tr>
                    <td>{this.props.client.fullname}</td>
                    <td>{this.props.client.birth} </td>
                    <td>{this.props.client.discount} </td>
                    <td>{this.props.client.passport} </td>
                </tr>

                <tr>
                    <td>Position </td>
                    <td>Name </td>
                    <td>Address </td>
                    <td>City </td>
                </tr>
                <tr>
                    <td>{text.position}</td>
                    <td>{text.fullname} </td>
                    <td>{this.state.shop.adress} </td>
                    <td>{this.state.shop.city} </td>
                </tr>

                </table>
                <div class="row">
                <p class="mr-sm-5 col-sm-4">Total price: {this.total_price()}</p>
                <p class="mr-sm-5 col-sm-4">Date: {new Date().toLocaleString()}</p>
                <button type="submit" onClick={this.sellCars.bind(this)} onSubmit={e=>e.preventDefault()} class="btn btn-primary mr-sm-5 col-sm-3" style={{height: 50}}>Sell</button>
                </div>
            </div>
        )
    }
}
