import React, { Component } from 'react'
import Select from 'react-select';
import {withRouter } from 'react-router-dom';

class SelectContainer extends Component {

    constructor(props) {
        super(props)
        this.state = {
            selectedOption: null
        }
    }
    
    handleChange = event => {
        this.setState({
            selectedOption: event.value,
        }, () => { console.log(this.state.selectedOption) 
            this.props.history.push(`/${this.state.selectedOption}`)
        })
    }
    
    render() {
        return (
            <Select
            placeholder  = {this.props.label}
            value = {this.state.selectedOption}
            onChange = {(e) => this.handleChange(e)}
            options = {this.props.options}
            />
        )
    }
}

export default withRouter(SelectContainer)
