import React, { Component } from 'react'
import {Config} from "../actions/api/config";
import ReactTable from 'react-table-6';
import "react-table-6/react-table.css";
import Order from './Order';

export default class ProviderTable extends Component {
    
    constructor(props) {
        super(props)
    
        this.toggleRowProvider = this.toggleRowProvider.bind(this);
        this.createOrder = this.createOrder.bind(this);
        this.recalculate = this.recalculate.bind(this);
        this.state = {

            selectedProvider: {},
            description: '',
            amount: null,
            order: null
        }
    }

    toggleRowProvider(firstName) {
        const newSelected = Object.assign({}, this.state.selectedProvider);
        this.props.providers.forEach(x => {
            newSelected[x.id] = false;
        });
        newSelected[firstName] = !this.state.selectedProvider[firstName];
        console.log(newSelected)
		this.setState({
			selectedProvider: newSelected,
		});
	}

    addStorage(car_id){
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(car_id+'~'+this.props.shop+'~'+'0')
        };
        console.log(options)
        fetch(`${Config.api_ip}/CarStorages`, options)
        .catch(console.log)
    }

    createOrder()
    {
        var car = this.props.car
        var car_storage = null 
        var provider = null

        for (let key in this.state.selectedProvider) {if(this.state.selectedProvider[key]) {provider = key; break;}}
        for (let key in this.props.storages) {
            if(this.props.storages[key].car==car.car) {car_storage = this.props.storages[key].storageId; break;}
        }
        if(car && provider && this.state.amount && this.state.amount > 0)
        {
            if(!car_storage)
            {
                this.addStorage(car.car)
            }
            var choosed_provider = this.props.providers.map(p => {if (p.id==provider){return p;}}).filter(function (el) {
                return el != null;
              })[0]
           this.setState({order: <Order car = {car} amount = {this.state.amount} description = {this.state.description}
                                    provider = {choosed_provider} storage = {car_storage} shop={this.props.shop}/>})
        }
        else {
            alert('Some mistake. Check choosed items')
        }
    }

    recalculate(e) {
        this.setState({
           description: e.target.value
        });
      }

    render() {

        const provider_columns = [{
            Header: "Providers",
            columns: [
                {
                    id: "checkbox",
                    accessor: "",
                    Cell: ({ original }) => {
                        return (
                            <input
                                style={{width: 40, minBlockSize:20}}
                                type="checkbox"
                                className="checkbox"
                                checked={this.state.selectedProvider[original.id] === true}
                                onChange={() => this.toggleRowProvider(original.id)}
                            />
                        );
                    },
                    Header: x => {
                        return (
                            <input
                                style={{width: 40, minBlockSize:20}}
                                type="checkbox"
                                className="checkbox"
                                checked="true"
                                disabled
                            />
                        );
                    },
                    filterable: false,
                    width: 45
                },
                {  
                   Header: 'Name',  
                   accessor: 'person',
                   filterable: false,  
                   },{  
                    Header: 'Phone',  
                    accessor: 'telephone',
                    filterable: false,  
                    },{  
                        Header: 'E-Mail',  
                        accessor: 'eMail',
                        filterable: false,  
                        },{  
                        Header: 'Adress',  
                        accessor: 'address',
                        filterable: false,  
                        }
            ]
        }];
        return (
            <div>
                    <ReactTable
                    data={this.props.providers}
                    columns={provider_columns}  
                    defaultPageSize = {1}  
                    pageSizeOptions = {[2,5,10]} 
                    filterable
                    filtered={this.state.filtered}
                    onFilteredChange={(filtered, column, value) => {
                        this.onFilteredChangeCustom(value, column.id || column.accessor);
                    }}
                    defaultFilterMethod={(filter, row, column) => {
                        const id = filter.pivotId || filter.id;
                        if (typeof filter.value === "object") {
                        return row[id] !== undefined
                            ? filter.value.indexOf(row[id]) > -1
                            : true;
                        } else {
                        return row[id] !== undefined
                            ? String(row[id]).indexOf(filter.value) > -1
                            : true;
                        }
                    }}/>
                    <br/>
                    <div class="row">
                    <button type="submit" onClick={this.createOrder} onSubmit={e=>e.preventDefault()} class="btn btn-primary mr-sm-5 mb-sm-5" style={{height: 50}}>Order</button>
                    <input
                    type="text" pattern="[0-9]*"
                    name="amount"
                    placeholder="Amount"
                    value={this.state.amount}
                    onChange={e => this.setState({amount: e.target.value})}
                    required
                    class="form-control mb-2 mr-sm-2 col-sm-4"
                    />
                    <textarea class=" mb-2 col-sm-4" name="description" value={this.state.description} onChange={this.recalculate} maxLength={200}
                    placeholder="Description. Don`t use: '~'!"/>
                </div>
                {this.state.order}
                    </div>
                
        )
    }
}
