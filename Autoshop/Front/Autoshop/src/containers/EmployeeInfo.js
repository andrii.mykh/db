import React, { Component } from 'react'
import { connect } from 'react-redux'
import {Config} from "../actions/api/config";

class EmployeeInfo extends Component {

    constructor(props) {
        super(props);
        this.state={
            employee: [String]
            }
    }

    async componentDidMount() {
        //console.log('data')
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const result = await fetch(`${Config.api_ip}/Employees/info_${this.props.log.logId}`, options)
        .then(response => response.json())
        .catch(console.log)

        let edited = result;
        edited.birth = result.birth.substring(0, 10)
        this.setState({employee: edited})
        localStorage.setItem('EmployeeInfo', JSON.stringify(this.state.employee))
    }

    render() {
        return (
            <div>
                <table class="table">
                <tr>
                    <td>Position </td>
                    <td>Name </td>
                    <td>Salary </td>
                    <td>Birth </td>
                    <td>Commition </td>
                </tr>
                <tr>
                    <td>{this.state.employee.position} </td>
                    <td>{this.state.employee.fullname} </td>
                    <td>{this.state.employee.salary} </td>
                    <td>{this.state.employee.birth} </td>
                    <td>{this.state.employee.commition} </td>
                </tr>
                </table>
            </div>
        )
    }
}

function mapStateToProps(state)
{
    return{
        log: state.logged
    }
}

export default connect(mapStateToProps)(EmployeeInfo)