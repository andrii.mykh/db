import React, { Component } from 'react'
import { connect } from 'react-redux'

class LoginNavbar extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        if(this.props.log.logId)
            return ([<h3>Log out</h3>])
        else return ([<h3>Log in</h3>])
    }
}

function mapStateToProps(state)
{
    return{
        log: state.logged
    }
}

export default connect(mapStateToProps)(LoginNavbar)