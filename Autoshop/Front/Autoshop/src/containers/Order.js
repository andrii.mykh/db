import React, { Component } from 'react'
import {Config} from "../actions/api/config";

export default class Order extends Component {

    constructor(props) {
        super(props)
        this.makeOrder = this.makeOrder.bind(this)
        this.state = {
             shop: [String],
             storage: {}
        }
    }
    

    async componentDidMount() {
        //console.log('data')
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const result = await fetch(`${Config.api_ip}/Shops/info_${this.props.shop}`, options)
        .then(response => response.json())
        .catch(console.log)
        this.setState({shop: result})
        
        const resultData = await fetch(`${Config.api_ip}/CarStorages/last`, options)
        .then(response => response.json())
        .catch(console.log)
        this.setState({storage: resultData})
    }

    makeOrder(){

        var data = new Date().toLocaleString()
        var storage = null
        if(this.props.storage) storage=this.props.storage
        else {
            storage = this.state.storage.idCarStorage
        }
        console.log('storage',storage+'~'+localStorage.getItem('logId')+'~'+
        this.props.provider.id+'~'+this.props.car.car+'~'+data+'~'+this.props.description+'~'+this.props.amount)
        
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(storage+'~'+localStorage.getItem('logId')+'~'+
            this.props.provider.id+'~'+this.props.car.car+'~'+data+'~'+this.props.description+'~'+this.props.amount)
        };
        console.log(options)
        fetch(`${Config.api_ip}/Orders`, options)
        .catch(console.log)
        window.location.reload();
    }

    render() {
        const text = JSON.parse(localStorage.getItem('EmployeeInfo'));
        return (
            <div class="col-sm-12">
                <table class="table">
                <tr>
                    <td>Amount </td>
                    <td>Brand </td>
                    <td>Model </td>
                    <td>Price </td>
                    <td>Year </td>
                    <td>Color </td>
                    <td>Types </td>
                </tr>
                <tr>
                    <td>{this.props.amount}</td>
                    <td>{this.props.car.brand} </td>
                    <td>{this.props.car.model} </td>
                    <td>{this.props.car.price} </td>
                    <td>{this.props.car.year} </td>
                    <td>{this.props.car.color} </td>
                    <td>{this.props.car.types} </td>
                </tr>

                <tr>
                    <td>Person </td>
                    <td>Brand </td>
                    <td>Phone </td>
                    <td>E-Mail </td>
                    <td>Address </td>
                </tr>
                <tr>
                    <td>{this.props.provider.person}</td>
                    <td>{this.props.provider.brand} </td>
                    <td>{this.props.provider.telephone} </td>
                    <td>{this.props.provider.eMail} </td>
                    <td>{this.props.provider.address} </td>
                </tr>

                <tr>
                    <td>Position </td>
                    <td>Name </td>
                    <td>Address </td>
                    <td>City </td>
                </tr>
                <tr>
                    <td>{text.position}</td>
                    <td>{text.fullname} </td>
                    <td>{this.state.shop.adress} </td>
                    <td>{this.state.shop.city} </td>
                </tr>

                </table>

                <p>Description: {this.props.description}</p>
                <div class="row">
                <p class="mr-sm-5 col-sm-4">Total price: {this.props.car.price*this.props.amount}</p>
                <p class="mr-sm-5 col-sm-4">Date: {new Date().toLocaleString()}</p>
                <button type="submit" onClick={this.makeOrder.bind(this)} onSubmit={e=>e.preventDefault()} class="btn btn-primary mb-5 mr-sm-5 col-sm-3" style={{height: 50}}>Make</button>
                </div>
            </div>
        )
    }
}
