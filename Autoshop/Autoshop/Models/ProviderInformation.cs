﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class ProviderInformation
    {
        //can`t create automaticly controler, because it doesn`t have primary key (virtual table)
        public int Id { get; set; }
        public string Person { get; set; }
        public string Telephone { get; set; }
        public string EMail { get; set; }
        public string Address { get; set; }
        public string Brand { get; set; }
    }
}
