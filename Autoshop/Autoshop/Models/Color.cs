﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class Color
    {
        public Color()
        {
            Car = new HashSet<Car>();
        }

        public int IdColor { get; set; }
        public string Color1 { get; set; }

        public virtual ICollection<Car> Car { get; set; }
    }
}
