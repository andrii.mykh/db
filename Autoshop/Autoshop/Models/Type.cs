﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class Type
    {
        public Type()
        {
            TypeCar = new HashSet<TypeCar>();
        }

        public int IdType { get; set; }
        public string Type1 { get; set; }

        public virtual ICollection<TypeCar> TypeCar { get; set; }
    }
}
