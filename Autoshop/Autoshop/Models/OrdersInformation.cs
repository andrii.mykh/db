﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class OrdersInformation
    {
        //can`t create automaticly controler, because it doesn`t have primary key (virtual table)
        public int Id { get; set; }
        public int Shop { get; set; }
        public int Employee { get; set; }
        public string EmployeeInfo { get; set; }
        public string Provider { get; set; }
        public string Car { get; set; }
        public int Amount { get; set; }
        public int Price { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public byte? IfAdded { get; set; }
        public DateTime? AddDate { get; set; }
    }
}
