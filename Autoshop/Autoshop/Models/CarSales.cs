﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class CarSales
    {
        public int IdEmployee { get; set; }
        public int IdClient { get; set; }
        public int IdCarStorage { get; set; }
        public int IdCarSales { get; set; }
        public DateTime? Date { get; set; }
        public int? TotalPrice { get; set; }

        public virtual CarStorage IdCarStorageNavigation { get; set; }
        public virtual Client IdClientNavigation { get; set; }
        public virtual Employee IdEmployeeNavigation { get; set; }
    }
}
