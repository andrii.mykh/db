﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class Shop
    {
        public Shop()
        {
            CarStorage = new HashSet<CarStorage>();
            Employee = new HashSet<Employee>();
        }

        public int IdCity { get; set; }
        public int IdShop { get; set; }
        public string Adress { get; set; }

        public virtual City IdCityNavigation { get; set; }
        public virtual ICollection<CarStorage> CarStorage { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
    }
}
