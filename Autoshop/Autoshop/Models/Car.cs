﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class Car
    {
        public Car()
        {
            CarStorage = new HashSet<CarStorage>();
            Orders = new HashSet<Orders>();
            TypeCar = new HashSet<TypeCar>();
        }

        public int IdBrand { get; set; }
        public int IdColor { get; set; }
        public int IdCar { get; set; }
        public string Model { get; set; }
        public DateTime? Year { get; set; }
        public int? Price { get; set; }

        public virtual Brand IdBrandNavigation { get; set; }
        public virtual Color IdColorNavigation { get; set; }
        public virtual ICollection<CarStorage> CarStorage { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }
        public virtual ICollection<TypeCar> TypeCar { get; set; }
    }
}
