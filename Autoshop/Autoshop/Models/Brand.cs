﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class Brand
    {
        public Brand()
        {
            Car = new HashSet<Car>();
            Provider = new HashSet<Provider>();
        }

        public int IdCountry { get; set; }
        public int IdBrand { get; set; }
        public string Brand1 { get; set; }

        public virtual Country IdCountryNavigation { get; set; }
        public virtual ICollection<Car> Car { get; set; }
        public virtual ICollection<Provider> Provider { get; set; }
    }
}
