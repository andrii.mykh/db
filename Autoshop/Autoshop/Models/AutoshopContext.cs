﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Autoshop.Models
{
    public partial class AutoshopContext : DbContext
    {
        public AutoshopContext()
        {
        }

        public AutoshopContext(DbContextOptions<AutoshopContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Brand> Brand { get; set; }
        public virtual DbSet<Car> Car { get; set; }
        public virtual DbSet<CarInformation> CarInformation { get; set; }
        public virtual DbSet<CarSales> CarSales { get; set; }

        public virtual DbSet<SellInformation> SellInformation { get; set; }
        public virtual DbSet<CarStorage> CarStorage { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<Color> Color { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<OrdersInformation> OrdersInformation { get; set; }
        public virtual DbSet<Position> Position { get; set; }
        public virtual DbSet<Provider> Provider { get; set; }
        public virtual DbSet<ProviderInformation> ProviderInformation { get; set; }
        public virtual DbSet<Shop> Shop { get; set; }
        public virtual DbSet<Type> Type { get; set; }
        public virtual DbSet<TypeCar> TypeCar { get; set; }

        //
        public virtual DbSet<EmployeeInfo> EmployeeInfo { get; set; }
        public virtual DbSet<ShopInfo> ShopInfo { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                // To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=ANDRII-DESKTOP-;Database=Autoshop;User Id=Andrii;Password=12345;Trusted_Connection=True;MultipleActiveResultSets=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Brand>(entity =>
            {
                entity.HasKey(e => e.IdBrand)
                    .HasName("XPKBrand");

                entity.HasIndex(e => e.IdCountry)
                    .HasName("XIF1Brand");

                entity.Property(e => e.IdBrand).HasColumnName("id_brand");

                entity.Property(e => e.Brand1)
                    .HasColumnName("brand")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdCountry).HasColumnName("id_country");

                entity.HasOne(d => d.IdCountryNavigation)
                    .WithMany(p => p.Brand)
                    .HasForeignKey(d => d.IdCountry)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_1");
            });

            modelBuilder.Entity<Car>(entity =>
            {
                entity.HasKey(e => e.IdCar)
                    .HasName("XPKCar");

                entity.HasIndex(e => e.IdBrand)
                    .HasName("XIF1Car");

                entity.HasIndex(e => e.IdColor)
                    .HasName("XIF3Car");

                entity.Property(e => e.IdCar).HasColumnName("id_car");

                entity.Property(e => e.IdBrand).HasColumnName("id_brand");

                entity.Property(e => e.IdColor).HasColumnName("id_color");

                entity.Property(e => e.Model)
                    .HasColumnName("model")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.Year)
                    .HasColumnName("year")
                    .HasColumnType("date");

                entity.HasOne(d => d.IdBrandNavigation)
                    .WithMany(p => p.Car)
                    .HasForeignKey(d => d.IdBrand)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_2");

                entity.HasOne(d => d.IdColorNavigation)
                    .WithMany(p => p.Car)
                    .HasForeignKey(d => d.IdColor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_16");
            });

            modelBuilder.Entity<CarInformation>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("Car_Information");

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.Brand)
                    .HasColumnName("brand")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Car).HasColumnName("car");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Model)
                    .HasColumnName("model")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Shop).HasColumnName("shop");

                entity.Property(e => e.StorageId).HasColumnName("storage_id");

                entity.Property(e => e.Types).HasColumnName("types");

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.Year).HasColumnName("year");
            });

            modelBuilder.Entity<CarSales>(entity =>
            {
                entity.HasKey(e => e.IdCarSales)
                    .HasName("XPKCar_Sales");

                entity.ToTable("Car_Sales");

                entity.HasIndex(e => e.IdCarStorage)
                    .HasName("XIF1Car_Sales");

                entity.HasIndex(e => e.IdClient)
                    .HasName("XIF3Car_Sales");

                entity.HasIndex(e => e.IdEmployee)
                    .HasName("XIF2Car_Sales");

                entity.Property(e => e.IdCarSales).HasColumnName("id_car_sales");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.IdCarStorage).HasColumnName("id_car_storage");

                entity.Property(e => e.IdClient).HasColumnName("id_client");

                entity.Property(e => e.IdEmployee).HasColumnName("id_employee");

                entity.Property(e => e.TotalPrice).HasColumnName("total_price");

                entity.HasOne(d => d.IdCarStorageNavigation)
                    .WithMany(p => p.CarSales)
                    .HasForeignKey(d => d.IdCarStorage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_10");

                entity.HasOne(d => d.IdClientNavigation)
                    .WithMany(p => p.CarSales)
                    .HasForeignKey(d => d.IdClient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_47");

                entity.HasOne(d => d.IdEmployeeNavigation)
                    .WithMany(p => p.CarSales)
                    .HasForeignKey(d => d.IdEmployee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_22");
            });

            modelBuilder.Entity<SellInformation>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("Sell_Information");

                entity.Property(e => e.EmployeeInfo)
                    .HasColumnName("employee_info")
                    .HasMaxLength(101)
                    .IsUnicode(false);

                entity.Property(e => e.Client)
                    .HasColumnName("client")
                    .HasMaxLength(126)
                    .IsUnicode(false);

                entity.Property(e => e.Car)
                    .HasColumnName("car")
                    //.HasMaxLength(162)
                    .IsUnicode(false);
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Shop).HasColumnName("shop");

                entity.Property(e => e.Employee).HasColumnName("employee");

                entity.Property(e => e.Total).HasColumnName("total");      

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<CarStorage>(entity =>
            {
                entity.HasKey(e => e.IdCarStorage)
                    .HasName("XPKCar_Storage");

                entity.ToTable("Car_Storage");

                entity.HasIndex(e => e.IdCar)
                    .HasName("XIF3Car_Storage");

                entity.HasIndex(e => e.IdShop)
                    .HasName("XIF4Car_Storage");

                entity.Property(e => e.IdCarStorage).HasColumnName("id_car_storage");

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.IdCar).HasColumnName("id_car");

                entity.Property(e => e.IdShop).HasColumnName("id_shop");

                entity.HasOne(d => d.IdCarNavigation)
                    .WithMany(p => p.CarStorage)
                    .HasForeignKey(d => d.IdCar)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_17");

                entity.HasOne(d => d.IdShopNavigation)
                    .WithMany(p => p.CarStorage)
                    .HasForeignKey(d => d.IdShop)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_9");
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.HasKey(e => e.IdCity)
                    .HasName("XPKCity");

                entity.HasIndex(e => e.IdCountry)
                    .HasName("XIF1City");

                entity.Property(e => e.IdCity).HasColumnName("id_city");

                entity.Property(e => e.City1)
                    .HasColumnName("city")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdCountry).HasColumnName("id_country");

                entity.HasOne(d => d.IdCountryNavigation)
                    .WithMany(p => p.City)
                    .HasForeignKey(d => d.IdCountry)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_6");
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.HasKey(e => e.IdClient)
                    .HasName("XPKClient");

                entity.Property(e => e.IdClient).HasColumnName("id_client");

                entity.Property(e => e.Birth)
                    .HasColumnName("birth")
                    .HasColumnType("date");

                entity.Property(e => e.Discount).HasColumnName("discount");

                entity.Property(e => e.Fullname)
                    .HasColumnName("fullname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Passport)
                    .HasColumnName("passport")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Color>(entity =>
            {
                entity.HasKey(e => e.IdColor)
                    .HasName("XPKColor");

                entity.Property(e => e.IdColor).HasColumnName("id_color");

                entity.Property(e => e.Color1)
                    .HasColumnName("color")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.HasKey(e => e.IdCountry)
                    .HasName("XPKCountry");

                entity.Property(e => e.IdCountry).HasColumnName("id_country");

                entity.Property(e => e.Country1)
                    .IsRequired()
                    .HasColumnName("country")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.IdEmployee)
                    .HasName("XPKEmployee");

                entity.HasIndex(e => e.IdPosition)
                    .HasName("XIF2Employee");

                entity.HasIndex(e => e.IdShop)
                    .HasName("XIF1Employee");

                entity.Property(e => e.IdEmployee).HasColumnName("id_employee");

                entity.Property(e => e.Birth)
                    .HasColumnName("birth")
                    .HasColumnType("date");

                entity.Property(e => e.Commition).HasColumnName("commition");

                entity.Property(e => e.Fullname)
                    .HasColumnName("fullname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdPosition).HasColumnName("id_position");

                entity.Property(e => e.IdShop).HasColumnName("id_shop");

                entity.Property(e => e.Passport)
                    .IsRequired()
                    .HasColumnName("passport")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Salary).HasColumnName("salary");

                entity.HasOne(d => d.IdPositionNavigation)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.IdPosition)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_21");

                entity.HasOne(d => d.IdShopNavigation)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.IdShop)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_29");
            });

            modelBuilder.Entity<EmployeeInfo>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Birth)
                    .HasColumnName("birth")
                    .HasColumnType("date");

                entity.Property(e => e.Commition).HasColumnName("commition");

                entity.Property(e => e.Fullname)
                    .HasColumnName("fullname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Salary).HasColumnName("salary");

                entity.Property(e => e.Position)
                   .HasColumnName("position")
                   .HasMaxLength(50)
                   .IsUnicode(false);

            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.HasKey(e => e.IdOrders)
                    .HasName("XPKTaking_In_Shop");

                entity.HasIndex(e => e.IdCar)
                    .HasName("XIF6Orders_Content");

                entity.HasIndex(e => e.IdCarStorage)
                    .HasName("XIF3Taking_In_Shop");

                entity.HasIndex(e => e.IdEmployee)
                    .HasName("XIF4Taking_In_Shop");

                entity.HasIndex(e => e.IdProvider)
                    .HasName("XIF1Orders");

                entity.Property(e => e.IdOrders).HasColumnName("id_orders");

                entity.Property(e => e.AddDate)
                    .HasColumnName("add_date")
                    .HasColumnType("date");

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdCar).HasColumnName("id_car");

                entity.Property(e => e.IdCarStorage).HasColumnName("id_car_storage");

                entity.Property(e => e.IdEmployee).HasColumnName("id_employee");

                entity.Property(e => e.IdProvider).HasColumnName("id_provider");

                entity.Property(e => e.IfAdded).HasColumnName("if_added");

                entity.HasOne(d => d.IdCarNavigation)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.IdCar)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_56");

                entity.HasOne(d => d.IdCarStorageNavigation)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.IdCarStorage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_45");

                entity.HasOne(d => d.IdEmployeeNavigation)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.IdEmployee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_50");

                entity.HasOne(d => d.IdProviderNavigation)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.IdProvider)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_14");
            });

            modelBuilder.Entity<OrdersInformation>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("Orders_Information");

                entity.Property(e => e.EmployeeInfo)
                    .HasColumnName("employee_info")
                    .HasMaxLength(101)
                    .IsUnicode(false);

                entity.Property(e => e.Provider)
                    .HasColumnName("provider")
                    .HasMaxLength(126)
                    .IsUnicode(false);

                entity.Property(e => e.Car)
                    .HasColumnName("car")
                    //.HasMaxLength(162)
                    .IsUnicode(false);
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Shop).HasColumnName("shop");

                entity.Property(e => e.Employee).HasColumnName("employee");

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.AddDate)
                    .HasColumnName("add_date")
                    .HasColumnType("date");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IfAdded).HasColumnName("if_added");
            });

            modelBuilder.Entity<Position>(entity =>
            {
                entity.HasKey(e => e.IdPosition)
                    .HasName("XPKPosition");

                entity.Property(e => e.IdPosition).HasColumnName("id_position");

                entity.Property(e => e.Position1)
                    .HasColumnName("position")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Provider>(entity =>
            {
                entity.HasKey(e => e.IdProvider)
                    .HasName("XPKProvider");

                entity.HasIndex(e => e.IdBrand)
                    .HasName("XIF2Provider");

                entity.HasIndex(e => e.IdCity)
                    .HasName("XIF1Provider");

                entity.Property(e => e.IdProvider).HasColumnName("id_provider");

                entity.Property(e => e.Adress)
                    .HasColumnName("adress")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EMail)
                    .HasColumnName("e_mail")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdBrand).HasColumnName("id_brand");

                entity.Property(e => e.IdCity).HasColumnName("id_city");

                entity.Property(e => e.Person)
                    .HasColumnName("person")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Telephone)
                    .HasColumnName("telephone")
                    .HasMaxLength(24)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdBrandNavigation)
                    .WithMany(p => p.Provider)
                    .HasForeignKey(d => d.IdBrand)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_46");

                entity.HasOne(d => d.IdCityNavigation)
                    .WithMany(p => p.Provider)
                    .HasForeignKey(d => d.IdCity)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_12");
            });

            modelBuilder.Entity<ProviderInformation>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("Provider_Information");

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasMaxLength(154)
                    .IsUnicode(false);

                entity.Property(e => e.Brand)
                    .HasColumnName("brand")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EMail)
                    .HasColumnName("e_mail")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Person)
                    .HasColumnName("person")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Telephone)
                    .HasColumnName("telephone")
                    .HasMaxLength(24)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Shop>(entity =>
            {
                entity.HasKey(e => e.IdShop)
                    .HasName("XPKShop");

                entity.HasIndex(e => e.IdCity)
                    .HasName("XIF1Shop");

                entity.Property(e => e.IdShop).HasColumnName("id_shop");

                entity.Property(e => e.Adress)
                    .HasColumnName("adress")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdCity).HasColumnName("id_city");

                entity.HasOne(d => d.IdCityNavigation)
                    .WithMany(p => p.Shop)
                    .HasForeignKey(d => d.IdCity)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_7");
            });

            modelBuilder.Entity<ShopInfo>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Adress)
                    .HasColumnName("adress")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                   .HasColumnName("city")
                   .HasMaxLength(50)
                   .IsUnicode(false);

            });

            modelBuilder.Entity<Type>(entity =>
            {
                entity.HasKey(e => e.IdType)
                    .HasName("XPKType");

                entity.Property(e => e.IdType).HasColumnName("id_type");

                entity.Property(e => e.Type1)
                    .HasColumnName("type")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TypeCar>(entity =>
            {
                entity.HasKey(e => new { e.IdType, e.IdCar })
                    .HasName("XPKType_Car");

                entity.ToTable("Type_Car");

                entity.HasIndex(e => e.IdCar)
                    .HasName("XIF2Color_Car");

                entity.HasIndex(e => e.IdType)
                    .HasName("XIF1Color_Car");

                entity.Property(e => e.IdType).HasColumnName("id_type");

                entity.Property(e => e.IdCar).HasColumnName("id_car");

                entity.HasOne(d => d.IdCarNavigation)
                    .WithMany(p => p.TypeCar)
                    .HasForeignKey(d => d.IdCar)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_61");

                entity.HasOne(d => d.IdTypeNavigation)
                    .WithMany(p => p.TypeCar)
                    .HasForeignKey(d => d.IdType)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("R_57");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
