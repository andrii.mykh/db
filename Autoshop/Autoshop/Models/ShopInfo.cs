﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Autoshop.Models
{
    public class ShopInfo
    {
        public string Adress { get; set; }
        public string City { get; set; }
    }
}
