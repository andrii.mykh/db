﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class Provider
    {
        public Provider()
        {
            Orders = new HashSet<Orders>();
        }

        public int IdCity { get; set; }
        public int IdBrand { get; set; }
        public int IdProvider { get; set; }
        public string Person { get; set; }
        public string Adress { get; set; }
        public string EMail { get; set; }
        public string Telephone { get; set; }

        public virtual Brand IdBrandNavigation { get; set; }
        public virtual City IdCityNavigation { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }
    }
}
