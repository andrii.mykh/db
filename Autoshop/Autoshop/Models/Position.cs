﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class Position
    {
        public Position()
        {
            Employee = new HashSet<Employee>();
        }

        public int IdPosition { get; set; }
        public string Position1 { get; set; }

        public virtual ICollection<Employee> Employee { get; set; }
    }
}
