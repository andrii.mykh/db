﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Autoshop.Models
{
    public partial class Employee
    {
        public Employee()
        {
            CarSales = new HashSet<CarSales>();
            Orders = new HashSet<Orders>();
        }
        public int IdShop { get; set; }
        public int IdPosition { get; set; }
        public int IdEmployee { get; set; }
        public string Fullname { get; set; }
        public int? Salary { get; set; }
        public DateTime? Birth { get; set; }
        public int? Commition { get; set; }
        public string Passport { get; set; }

        public virtual Position IdPositionNavigation { get; set; }
        public virtual Shop IdShopNavigation { get; set; }
        public virtual ICollection<CarSales> CarSales { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }
    }
}
