﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class TypeCar
    {
        //classical controler was updated, because it has a pair key
        public int IdType { get; set; }
        public int IdCar { get; set; }

        public virtual Car IdCarNavigation { get; set; }
        public virtual Type IdTypeNavigation { get; set; }
    }
}
