﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class City
    {
        public City()
        {
            Provider = new HashSet<Provider>();
            Shop = new HashSet<Shop>();
        }

        public int IdCountry { get; set; }
        public int IdCity { get; set; }
        public string City1 { get; set; }

        public virtual Country IdCountryNavigation { get; set; }
        public virtual ICollection<Provider> Provider { get; set; }
        public virtual ICollection<Shop> Shop { get; set; }
    }
}
