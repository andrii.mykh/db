﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class Client
    {
        public Client()
        {
            CarSales = new HashSet<CarSales>();
        }

        public int IdClient { get; set; }
        public string Fullname { get; set; }
        public DateTime? Birth { get; set; }
        public int? Discount { get; set; }
        public string Passport { get; set; }

        public virtual ICollection<CarSales> CarSales { get; set; }
    }
}
