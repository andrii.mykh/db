﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Autoshop.Models
{
    public class EmployeeInfo
    {
        public string Position { get; set; }
        public string Fullname { get; set; }
        public int Salary { get; set; }
        public DateTime Birth { get; set; }
        public int Commition { get; set; }
    }
}
