﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class Country
    {
        public Country()
        {
            Brand = new HashSet<Brand>();
            City = new HashSet<City>();
        }

        public int IdCountry { get; set; }
        public string Country1 { get; set; }

        public virtual ICollection<Brand> Brand { get; set; }
        public virtual ICollection<City> City { get; set; }
    }
}
