﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class CarInformation
    {
        //can`t create automaticly controler, because it doesn`t have primary key (virtual table)
        public int StorageId { get; set; }
        public int Car { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public int? Price { get; set; }
        public int? Year { get; set; }
        public string Color { get; set; }
        public string Types { get; set; }
        public int? Amount { get; set; }
        public int Shop { get; set; }
    }
}
