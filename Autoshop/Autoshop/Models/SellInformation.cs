﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class SellInformation
    {
        //can`t create automaticly controler, because it doesn`t have primary key (virtual table)
        public int Id { get; set; }
        public int Shop { get; set; }
        public int Employee { get; set; }
        public string EmployeeInfo { get; set; }
        public string Client { get; set; }
        public string Car { get; set; }
        public DateTime Date { get; set; }

        public int Total { get; set; }
    }
}
