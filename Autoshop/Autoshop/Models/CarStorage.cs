﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class CarStorage
    {
        public CarStorage()
        {
            CarSales = new HashSet<CarSales>();
            Orders = new HashSet<Orders>();
        }

        public int IdCar { get; set; }
        public int IdShop { get; set; }
        public int IdCarStorage { get; set; }
        public int? Amount { get; set; }

        public virtual Car IdCarNavigation { get; set; }
        public virtual Shop IdShopNavigation { get; set; }
        public virtual ICollection<CarSales> CarSales { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }
    }
}
