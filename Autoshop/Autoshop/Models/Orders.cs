﻿using System;
using System.Collections.Generic;

namespace Autoshop.Models
{
    public partial class Orders
    {
        public int IdCarStorage { get; set; }
        public int IdEmployee { get; set; }
        public int IdProvider { get; set; }
        public int IdCar { get; set; }
        public int? Amount { get; set; }
        public DateTime? Date { get; set; }
        public byte? IfAdded { get; set; }
        public DateTime? AddDate { get; set; }
        public string Description { get; set; }
        public int IdOrders { get; set; }

        public virtual Car IdCarNavigation { get; set; }
        public virtual CarStorage IdCarStorageNavigation { get; set; }
        public virtual Employee IdEmployeeNavigation { get; set; }
        public virtual Provider IdProviderNavigation { get; set; }
    }
}
