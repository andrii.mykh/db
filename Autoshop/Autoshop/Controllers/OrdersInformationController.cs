﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Autoshop.Models;
using Microsoft.AspNetCore.Cors;

namespace Autoshop.Controllers
{
    [EnableCors("myAllowSpecificOrigins")]
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersInformationController : ControllerBase
    {
        private readonly AutoshopContext _context;

        public OrdersInformationController(AutoshopContext context)
        {
            _context = context;
        }

        // GET: api/OrdersInformation
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrdersInformation>>> GetOrdersInfo()
        {
            return await _context.OrdersInformation.ToListAsync();
        }

        // GET: api/OrdersInformation/5
        [HttpGet("{id}")]
        public async Task<ActionResult<OrdersInformation>> GetOrdersInfo(int id)
        {
            var provider = await _context.OrdersInformation.FirstOrDefaultAsync(e => e.Id == id);

            if (provider == null)
            {
                return NotFound();
            }

            return provider;
        }
    }
}
