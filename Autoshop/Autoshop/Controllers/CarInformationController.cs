﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Autoshop.Models;
using Microsoft.AspNetCore.Cors;

namespace Autoshop.Controllers
{
    [EnableCors("myAllowSpecificOrigins")]
    [Route("api/[controller]")]
    [ApiController]
    public class CarInformationController : ControllerBase
    {
        private readonly AutoshopContext _context;

        public CarInformationController(AutoshopContext context)
        {
            _context = context;
        }

        // GET: api/CarInformation
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CarInformation>>> GetCarInfo()
        {
            return await _context.CarInformation.ToListAsync();
        }

        // GET: api/CarInformation/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CarInformation>> GetCarInfo(int id)
        {
            var car = await _context.CarInformation.FirstOrDefaultAsync(e => e.StorageId == id);

            if (car == null)
            {
                return NotFound();
            }

            return car;
        }
    }
}
