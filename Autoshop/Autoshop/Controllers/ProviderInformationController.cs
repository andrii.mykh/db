﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Autoshop.Models;
using Microsoft.AspNetCore.Cors;

namespace Autoshop.Controllers
{
    [EnableCors("myAllowSpecificOrigins")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProviderInformationController : ControllerBase
    {
        private readonly AutoshopContext _context;

        public ProviderInformationController(AutoshopContext context)
        {
            _context = context;
        }

        // GET: api/ProviderInformation
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProviderInformation>>> GetProviderInfo()
        {
            return await _context.ProviderInformation.ToListAsync();
        }

        // GET: api/ProviderInformation/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProviderInformation>> GetProviderInfo(int id)
        {
            var provider = await _context.ProviderInformation.FirstOrDefaultAsync(e => e.Id == id);

            if (provider == null)
            {
                return NotFound();
            }

            return provider;
        }
    }
}
