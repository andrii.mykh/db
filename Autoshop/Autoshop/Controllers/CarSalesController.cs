﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Autoshop.Models;
using Microsoft.AspNetCore.Cors;
using System.Globalization;

namespace Autoshop.Controllers
{
    [EnableCors("myAllowSpecificOrigins")]
    [Route("api/[controller]")]
    [ApiController]
    public class CarSalesController : ControllerBase
    {
        private readonly AutoshopContext _context;

        public CarSalesController(AutoshopContext context)
        {
            _context = context;
        }

        // GET: api/CarSales
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CarSales>>> GetCarSales()
        {
            return await _context.CarSales.ToListAsync();
        }

        // GET: api/CarSales/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CarSales>> GetCarSales(int id)
        {
            var carSales = await _context.CarSales.FindAsync(id);

            if (carSales == null)
            {
                return NotFound();
            }

            return carSales;
        }

        // PUT: api/CarSales/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCarSales(int id, CarSales carSales)
        {
            //if (id != carSales.IdCarSales)
            //{
            //    return BadRequest();
            //}

            carSales.IdCarSales = id;


            _context.Entry(carSales).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CarSalesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CarSales
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [EnableCors("myAllowSpecificOrigins")]
        [HttpPost]
        public async Task<ActionResult<CarSales>> PostCarSales([FromBody] string data)
        {
            string[] parts = data.Split('~');

            string[] validformats = new[] { "MM/dd/yyyy", "yyyy/MM/dd", "MM/dd/yyyy HH:mm:ss",
                                        "MM/dd/yyyy hh:mm tt", "yyyy-MM-dd HH:mm:ss,fff", "yyyy-MM-dd", "dd.MM.yyyy, HH:mm:ss" };
            CultureInfo provider = new CultureInfo("uk-UA");

            CarSales carSales = new CarSales();
            carSales.IdEmployee = int.Parse(parts[0]);
            carSales.IdClient = int.Parse(parts[1]);
            carSales.IdCarStorage = int.Parse(parts[2]);
            carSales.Date = DateTime.ParseExact(parts[3], validformats, provider);
            carSales.TotalPrice = int.Parse(parts[4]);
            _context.CarSales.Add(carSales);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCarSales", new { id = carSales.IdCarSales }, carSales);
        }

        // DELETE: api/CarSales/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CarSales>> DeleteCarSales(int id)
        {
            var carSales = await _context.CarSales.FindAsync(id);
            if (carSales == null)
            {
                return NotFound();
            }

            _context.CarSales.Remove(carSales);
            await _context.SaveChangesAsync();

            return carSales;
        }

        private bool CarSalesExists(int id)
        {
            return _context.CarSales.Any(e => e.IdCarSales == id);
        }
    }
}
