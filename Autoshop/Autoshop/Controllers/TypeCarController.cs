﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Autoshop.Models;
using Microsoft.AspNetCore.Cors;

namespace Autoshop.Controllers
{
    [EnableCors("myAllowSpecificOrigins")]
    [Route("api/[controller]")]
    [ApiController]
    public class TypeCarController : ControllerBase
    {
        private readonly AutoshopContext _context;

        public TypeCarController(AutoshopContext context)
        {
            _context = context;
        }

        // GET: api/TypeCar
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TypeCar>>> GetTypeCar()
        {
            return await _context.TypeCar.ToListAsync();
        }

        // GET: api/TypeCar/1_1
        [HttpGet("{idT}_{idC}")]
        public async Task<ActionResult<TypeCar>> GetTypeCar(int idT, int idC)
        {
            var typeCar = await _context.TypeCar.FindAsync(idT, idC);

            if (typeCar == null)
            {
                return NotFound();
            }

            return typeCar;
        }

        // PUT: api/TypeCar/5_5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{idT}_{idC}")]
        public async Task<IActionResult> PutTypeCar(int idT, int idC, TypeCar typeCar)
        {
            //if ((idT != typeCar.IdType)&& (idC != typeCar.IdCar))
            //{
            //    return BadRequest();
            //}

            typeCar.IdType = idT;
            typeCar.IdCar = idC;

            _context.Entry(typeCar).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TypeCarExists(idT, idC))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TypeCar
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<TypeCar>> PostTypeCar(TypeCar typeCar)
        {
            _context.TypeCar.Add(typeCar);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TypeCarExists(typeCar.IdType, typeCar.IdCar))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTypeCar", new { idT = typeCar.IdType }, new { idC = typeCar.IdCar });
        }

        // DELETE: api/TypeCar/5_5
        [HttpDelete("{idT}_{idC}")]
        public async Task<ActionResult<TypeCar>> DeleteTypeCar(int idT, int idC)
        {
            var typeCar = await _context.TypeCar.FindAsync(idT, idC);
            if (typeCar == null)
            {
                return NotFound();
            }

            _context.TypeCar.Remove(typeCar);
            await _context.SaveChangesAsync();

            return typeCar;
        }

        private bool TypeCarExists(int idT,int idC)
        {
            return _context.TypeCar.Any(e => (e.IdType == idT && e.IdCar == idC));
        }
    }
}
