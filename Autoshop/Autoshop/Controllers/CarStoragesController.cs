﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Autoshop.Models;
using Microsoft.AspNetCore.Cors;

namespace Autoshop.Controllers
{
    [EnableCors("myAllowSpecificOrigins")]
    [Route("api/[controller]")]
    [ApiController]
    public class CarStoragesController : ControllerBase
    {
        private readonly AutoshopContext _context;

        public CarStoragesController(AutoshopContext context)
        {
            _context = context;
        }

        // GET: api/CarStorages
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CarStorage>>> GetCarStorage()
        {
            return await _context.CarStorage.ToListAsync();
        }

        [HttpGet("last")]
        public async Task<ActionResult<CarStorage>> GetCarStorageLast()
        {
            var last_storage = await _context.CarStorage.FromSqlRaw($"exec [dbo].[Last_Storage]").ToListAsync();
            return last_storage.FirstOrDefault();
        }

        // GET: api/CarStorages/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CarStorage>> GetCarStorage(int id)
        {
            var carStorage = await _context.CarStorage.FindAsync(id);

            if (carStorage == null)
            {
                return NotFound();
            }

            return carStorage;
        }

        // PUT: api/CarStorages/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCarStorage(int id, CarStorage carStorage)
        {
            //if (id != carStorage.IdCarStorage)
            //{
            //    return BadRequest();
            //}

            carStorage.IdCarStorage = id;


            _context.Entry(carStorage).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CarStorageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CarStorages
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CarStorage>> PostCarStorage([FromBody] string data)
        {
            string[] parts = data.Split('~');

            CarStorage carStorage = new CarStorage();
            carStorage.IdCar = int.Parse(parts[0]);
            carStorage.IdShop = int.Parse(parts[1]);
            carStorage.Amount = int.Parse(parts[2]); 
            _context.CarStorage.Add(carStorage);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCarStorage", new { id = carStorage.IdCarStorage }, carStorage);
        }

        // DELETE: api/CarStorages/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CarStorage>> DeleteCarStorage(int id)
        {
            var carStorage = await _context.CarStorage.FindAsync(id);
            if (carStorage == null)
            {
                return NotFound();
            }

            _context.CarStorage.Remove(carStorage);
            await _context.SaveChangesAsync();

            return carStorage;
        }

        private bool CarStorageExists(int id)
        {
            return _context.CarStorage.Any(e => e.IdCarStorage == id);
        }
    }
}
