﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Autoshop.Models;
using Microsoft.AspNetCore.Cors;
using System.Globalization;
using System.Data.SqlClient;

namespace Autoshop.Controllers
{
    [EnableCors("myAllowSpecificOrigins")]
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly AutoshopContext _context;

        public OrdersController(AutoshopContext context)
        {
            _context = context;
        }

        // GET: api/Orders
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Orders>>> GetOrders()
        {
            return await _context.Orders.ToListAsync();
        }

        // GET: api/Orders/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Orders>> GetOrders(int id)
        {
            var orders = await _context.Orders.FindAsync(id);

            if (orders == null)
            {
                return NotFound();
            }

            return orders;
        }

        // PUT: api/Orders/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrders(int id, [FromBody] byte data)
        {
            //if (id != orders.IdOrders)
            //{
            //    return BadRequest();
            //}

            //_context.Entry(orders).State = EntityState.Modified;

            if (!OrdersExists(id))
            {
                return BadRequest();
            }

            try
            {
                await _context.Database.ExecuteSqlCommandAsync($"exec [dbo].[Uppdate_Orders] @if= {data}, @order_id= {id}");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrdersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Orders
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Orders>> PostOrders([FromBody] string data)
        {
            string[] parts = data.Split('~');
            string[] validformats = new[] { "MM/dd/yyyy", "yyyy/MM/dd", "MM/dd/yyyy HH:mm:ss",
                                        "MM/dd/yyyy hh:mm tt", "yyyy-MM-dd HH:mm:ss,fff", "yyyy-MM-dd", "dd.MM.yyyy, HH:mm:ss" };
            CultureInfo provider = new CultureInfo("uk-UA");

            Orders orders = new Orders();
            orders.IdCarStorage = int.Parse(parts[0]);
            orders.IdEmployee = int.Parse(parts[1]);
            orders.IdProvider = int.Parse(parts[2]);
            orders.IdCar = int.Parse(parts[3]);
            orders.Date = DateTime.ParseExact(parts[4], validformats, provider);
            orders.Description = parts[5];
            orders.Amount = int.Parse(parts[6]);

            _context.Orders.Add(orders);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetOrders", new { id = orders.IdOrders }, orders);
        }

        // DELETE: api/Orders/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Orders>> DeleteOrders(int id)
        {
            var orders = await _context.Orders.FindAsync(id);
            if (orders == null)
            {
                return NotFound();
            }

            _context.Orders.Remove(orders);
            await _context.SaveChangesAsync();

            return orders;
        }

        private bool OrdersExists(int id)
        {
            return _context.Orders.Any(e => e.IdOrders == id);
        }
    }
}
