﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Autoshop.Migrations
{
    public partial class Connection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Client",
                columns: table => new
                {
                    id_client = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    fullname = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    birth = table.Column<DateTime>(type: "date", nullable: true),
                    discount = table.Column<int>(nullable: true),
                    passport = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKClient", x => x.id_client);
                });

            migrationBuilder.CreateTable(
                name: "Color",
                columns: table => new
                {
                    id_color = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    color = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKColor", x => x.id_color);
                });

            migrationBuilder.CreateTable(
                name: "Country",
                columns: table => new
                {
                    id_country = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    country = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKCountry", x => x.id_country);
                });

            migrationBuilder.CreateTable(
                name: "Position",
                columns: table => new
                {
                    id_position = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    position = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKPosition", x => x.id_position);
                });

            migrationBuilder.CreateTable(
                name: "Type",
                columns: table => new
                {
                    id_type = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    type = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKType", x => x.id_type);
                });

            migrationBuilder.CreateTable(
                name: "Brand",
                columns: table => new
                {
                    id_brand = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_country = table.Column<int>(nullable: false),
                    brand = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKBrand", x => x.id_brand);
                    table.ForeignKey(
                        name: "R_1",
                        column: x => x.id_country,
                        principalTable: "Country",
                        principalColumn: "id_country",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "City",
                columns: table => new
                {
                    id_city = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_country = table.Column<int>(nullable: false),
                    city = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKCity", x => x.id_city);
                    table.ForeignKey(
                        name: "R_6",
                        column: x => x.id_country,
                        principalTable: "Country",
                        principalColumn: "id_country",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Car",
                columns: table => new
                {
                    id_car = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_brand = table.Column<int>(nullable: false),
                    id_color = table.Column<int>(nullable: false),
                    model = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    year = table.Column<DateTime>(type: "date", nullable: true),
                    price = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKCar", x => x.id_car);
                    table.ForeignKey(
                        name: "R_2",
                        column: x => x.id_brand,
                        principalTable: "Brand",
                        principalColumn: "id_brand",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "R_16",
                        column: x => x.id_color,
                        principalTable: "Color",
                        principalColumn: "id_color",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Provider",
                columns: table => new
                {
                    id_provider = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_city = table.Column<int>(nullable: false),
                    id_brand = table.Column<int>(nullable: false),
                    person = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    adress = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    e_mail = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    telephone = table.Column<string>(unicode: false, maxLength: 24, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKProvider", x => x.id_provider);
                    table.ForeignKey(
                        name: "R_46",
                        column: x => x.id_brand,
                        principalTable: "Brand",
                        principalColumn: "id_brand",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "R_12",
                        column: x => x.id_city,
                        principalTable: "City",
                        principalColumn: "id_city",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Shop",
                columns: table => new
                {
                    id_shop = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_city = table.Column<int>(nullable: false),
                    adress = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKShop", x => x.id_shop);
                    table.ForeignKey(
                        name: "R_7",
                        column: x => x.id_city,
                        principalTable: "City",
                        principalColumn: "id_city",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Type_Car",
                columns: table => new
                {
                    id_type = table.Column<int>(nullable: false),
                    id_car = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKType_Car", x => new { x.id_type, x.id_car });
                    table.ForeignKey(
                        name: "R_61",
                        column: x => x.id_car,
                        principalTable: "Car",
                        principalColumn: "id_car",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "R_57",
                        column: x => x.id_type,
                        principalTable: "Type",
                        principalColumn: "id_type",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Car_Storage",
                columns: table => new
                {
                    id_car_storage = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_car = table.Column<int>(nullable: false),
                    id_shop = table.Column<int>(nullable: false),
                    amount = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKCar_Storage", x => x.id_car_storage);
                    table.ForeignKey(
                        name: "R_17",
                        column: x => x.id_car,
                        principalTable: "Car",
                        principalColumn: "id_car",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "R_9",
                        column: x => x.id_shop,
                        principalTable: "Shop",
                        principalColumn: "id_shop",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employee",
                columns: table => new
                {
                    id_employee = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_shop = table.Column<int>(nullable: false),
                    id_position = table.Column<int>(nullable: false),
                    fullname = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    salary = table.Column<int>(nullable: true),
                    birth = table.Column<DateTime>(type: "date", nullable: true),
                    commition = table.Column<int>(nullable: true),
                    passport = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKEmployee", x => x.id_employee);
                    table.ForeignKey(
                        name: "R_21",
                        column: x => x.id_position,
                        principalTable: "Position",
                        principalColumn: "id_position",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "R_29",
                        column: x => x.id_shop,
                        principalTable: "Shop",
                        principalColumn: "id_shop",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Car_Sales",
                columns: table => new
                {
                    id_car_sales = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_employee = table.Column<int>(nullable: false),
                    id_client = table.Column<int>(nullable: false),
                    id_car_storage = table.Column<int>(nullable: false),
                    date = table.Column<DateTime>(type: "date", nullable: true),
                    total_price = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKCar_Sales", x => x.id_car_sales);
                    table.ForeignKey(
                        name: "R_10",
                        column: x => x.id_car_storage,
                        principalTable: "Car_Storage",
                        principalColumn: "id_car_storage",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "R_47",
                        column: x => x.id_client,
                        principalTable: "Client",
                        principalColumn: "id_client",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "R_22",
                        column: x => x.id_employee,
                        principalTable: "Employee",
                        principalColumn: "id_employee",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    id_orders = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    id_car_storage = table.Column<int>(nullable: false),
                    id_employee = table.Column<int>(nullable: false),
                    id_provider = table.Column<int>(nullable: false),
                    id_car = table.Column<int>(nullable: false),
                    amount = table.Column<int>(nullable: true),
                    date = table.Column<DateTime>(type: "date", nullable: true),
                    if_added = table.Column<byte>(nullable: true),
                    add_date = table.Column<DateTime>(type: "date", nullable: true),
                    description = table.Column<string>(unicode: false, maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKTaking_In_Shop", x => x.id_orders);
                    table.ForeignKey(
                        name: "R_56",
                        column: x => x.id_car,
                        principalTable: "Car",
                        principalColumn: "id_car",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "R_45",
                        column: x => x.id_car_storage,
                        principalTable: "Car_Storage",
                        principalColumn: "id_car_storage",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "R_50",
                        column: x => x.id_employee,
                        principalTable: "Employee",
                        principalColumn: "id_employee",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "R_14",
                        column: x => x.id_provider,
                        principalTable: "Provider",
                        principalColumn: "id_provider",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "XIF1Brand",
                table: "Brand",
                column: "id_country");

            migrationBuilder.CreateIndex(
                name: "XIF1Car",
                table: "Car",
                column: "id_brand");

            migrationBuilder.CreateIndex(
                name: "XIF3Car",
                table: "Car",
                column: "id_color");

            migrationBuilder.CreateIndex(
                name: "XIF1Car_Sales",
                table: "Car_Sales",
                column: "id_car_storage");

            migrationBuilder.CreateIndex(
                name: "XIF3Car_Sales",
                table: "Car_Sales",
                column: "id_client");

            migrationBuilder.CreateIndex(
                name: "XIF2Car_Sales",
                table: "Car_Sales",
                column: "id_employee");

            migrationBuilder.CreateIndex(
                name: "XIF3Car_Storage",
                table: "Car_Storage",
                column: "id_car");

            migrationBuilder.CreateIndex(
                name: "XIF4Car_Storage",
                table: "Car_Storage",
                column: "id_shop");

            migrationBuilder.CreateIndex(
                name: "XIF1City",
                table: "City",
                column: "id_country");

            migrationBuilder.CreateIndex(
                name: "XIF2Employee",
                table: "Employee",
                column: "id_position");

            migrationBuilder.CreateIndex(
                name: "XIF1Employee",
                table: "Employee",
                column: "id_shop");

            migrationBuilder.CreateIndex(
                name: "XIF6Orders_Content",
                table: "Orders",
                column: "id_car");

            migrationBuilder.CreateIndex(
                name: "XIF3Taking_In_Shop",
                table: "Orders",
                column: "id_car_storage");

            migrationBuilder.CreateIndex(
                name: "XIF4Taking_In_Shop",
                table: "Orders",
                column: "id_employee");

            migrationBuilder.CreateIndex(
                name: "XIF1Orders",
                table: "Orders",
                column: "id_provider");

            migrationBuilder.CreateIndex(
                name: "XIF2Provider",
                table: "Provider",
                column: "id_brand");

            migrationBuilder.CreateIndex(
                name: "XIF1Provider",
                table: "Provider",
                column: "id_city");

            migrationBuilder.CreateIndex(
                name: "XIF1Shop",
                table: "Shop",
                column: "id_city");

            migrationBuilder.CreateIndex(
                name: "XIF2Color_Car",
                table: "Type_Car",
                column: "id_car");

            migrationBuilder.CreateIndex(
                name: "XIF1Color_Car",
                table: "Type_Car",
                column: "id_type");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Car_Sales");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Type_Car");

            migrationBuilder.DropTable(
                name: "Client");

            migrationBuilder.DropTable(
                name: "Car_Storage");

            migrationBuilder.DropTable(
                name: "Employee");

            migrationBuilder.DropTable(
                name: "Provider");

            migrationBuilder.DropTable(
                name: "Type");

            migrationBuilder.DropTable(
                name: "Car");

            migrationBuilder.DropTable(
                name: "Position");

            migrationBuilder.DropTable(
                name: "Shop");

            migrationBuilder.DropTable(
                name: "Brand");

            migrationBuilder.DropTable(
                name: "Color");

            migrationBuilder.DropTable(
                name: "City");

            migrationBuilder.DropTable(
                name: "Country");
        }
    }
}
